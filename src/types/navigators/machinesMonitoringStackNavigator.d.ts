type MachinesMonitoringScreenStack = {
  Monitoring: undefined;
  Info: { machine: Machine };
  MachineNotStarted: { machine: Machine };
};
