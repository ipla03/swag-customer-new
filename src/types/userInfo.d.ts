interface UserInfo {
  id: string;
  authorities: UserAuthority;
  email: string;
  firstName: string;
  idHash?: string;
  lastName: string;
  profilePicture: string;
  promoterCode?: string;
  username: string;
}
