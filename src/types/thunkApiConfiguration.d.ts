import { RootState } from "./../redux/store";

interface ThunkApiConfiguration {
  state: RootState;
  rejectValue: string;
}
