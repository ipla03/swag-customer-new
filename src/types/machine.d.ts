interface Machine {
  id: string;
  name: string;
  fraction: number;
  production: number;
  dailyProduction: number;
  averageDailyProduction: number;
  miningEndDate: string;
  miningStartDate: string;
  status: MachineStatus;
  hashRate: number;
  machineType: string;
  serialNumber: string;
}

type MachineStatus =
  | 'READY_FOR_SHIPPING'
  | 'SHIPPED'
  | 'SETTING_UP'
  | 'PRODUCING';
