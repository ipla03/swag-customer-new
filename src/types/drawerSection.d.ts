interface DrawerSection {
  title: string;
  backgroundColor: string;
  screenName: string;
  comingSoon?: boolean;
}
