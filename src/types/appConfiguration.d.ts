interface AppConfiguration {
  minVersion?: string;
  maintenance?: Maintenance;
  addMachineLink?: string;
}

interface Maintenance {
  enabled: boolean;
  info: string;
}
