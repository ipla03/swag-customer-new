interface GetJwtResponse {
  authorities: UserAuthority[];
  jwt: string;
  username: string;
}
