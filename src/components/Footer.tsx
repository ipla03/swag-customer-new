import React from "react";
import { View, Text, StyleSheet, Dimensions, Platform } from "react-native";
import { Colors } from "@styles";
import Svg, { Polygon } from "react-native-svg";
import Constants from "expo-constants";

const STATUS_BAR_HEIGHT = Constants.statusBarHeight;
const SMALL_DEVICE = Dimensions.get("window").height < 600;
const WIDTH = Dimensions.get("window").width;
const FOOTER_HEIGHT = (SMALL_DEVICE ? 75 : 100) + STATUS_BAR_HEIGHT;
const FOOTER_LOWER_SIDE_DIFFERENCE = 20;
const OVERFLOW_HEIGHT = Dimensions.get("window").height;

const renderSvgFooter = () => {
  const points = `
    0,${FOOTER_LOWER_SIDE_DIFFERENCE},
    ${WIDTH},0 ${WIDTH},
    ${FOOTER_HEIGHT + OVERFLOW_HEIGHT} 0,
    ${FOOTER_HEIGHT + OVERFLOW_HEIGHT}`;

  if (Platform.OS == "android") {
    return (
      <View
        style={{
          width: "100%",
          ...styles.headerShadow,
          position: "absolute",
          bottom: -OVERFLOW_HEIGHT,
        }}
      >
        <Svg width="100%" height={FOOTER_HEIGHT + OVERFLOW_HEIGHT}>
          <Polygon points={points} fill={Colors.primary} />
        </Svg>
      </View>
    );
  }

  return (
    <Svg
      width="100%"
      height={FOOTER_HEIGHT + OVERFLOW_HEIGHT}
      style={{
        position: "absolute",
        bottom: -OVERFLOW_HEIGHT,
        ...styles.headerShadow,
      }}
    >
      <Polygon points={points} fill={Colors.primary} />
    </Svg>
  );
};

function Footer() {
  return <View style={styles.container}>{renderSvgFooter()}</View>;
}

const styles = StyleSheet.create({
  container: {
    width: WIDTH,
    height: FOOTER_HEIGHT,
    alignItems: "center",
    justifyContent: "center",
  },
  headerShadow: {
    shadowOpacity: Platform.OS == "ios" ? 0.4 : 0.7,
    shadowRadius: Platform.OS == "ios" ? 10 : 5,
    shadowColor: "black",
  },
});

export default Footer;
