import React, { ReactNode } from 'react';
import { Views } from '@styles';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import ProfileButton from '@assets/images/profile_button.svg';
import { Image } from 'react-native';
import MenuButton from '@assets/images/menu_button.svg';
import Header from './Header';

interface Props {
  profilePictureUrl?: string;
  navigateToProfile: () => void;
  openMenu: () => void;
  alternativeMenu?: ReactNode;
}

function HeaderWithMenuAndProfileButton({
  profilePictureUrl,
  navigateToProfile,
  openMenu,
  alternativeMenu,
}: Props) {
  const renderProfileButton = () => {
    return (
      <View style={Views.fillAndCenter}>
        <TouchableOpacity
          style={Views.fillAndCenter}
          onPress={navigateToProfile}>
          <View style={Views.fillAndCenter}>
            <ProfileButton
              style={{ position: 'absolute' }}
              width="60%"
              height="60%"
            />
            <View style={styles.profilePictureContainer}>
              <Image
                style={styles.profilePicture}
                source={{ uri: profilePictureUrl }}
                resizeMode="contain"
              />
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  const renderDrawerButton = () => {
    if (alternativeMenu) {
      return alternativeMenu;
    }

    return (
      <View style={Views.fillAndCenter}>
        <TouchableOpacity onPress={openMenu}>
          <MenuButton width={24} height={24} />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <Header
      leftButton={renderDrawerButton()}
      rightButton={renderProfileButton()}
    />
  );
}

const styles = StyleSheet.create({
  profilePictureContainer: {
    height: '55%',
    aspectRatio: 1,
    ...(Views.center as object),
  },
  profilePicture: {
    height: '100%',
    width: '100%',
    borderRadius: 1000,
  },
});

export default HeaderWithMenuAndProfileButton;
