import React from "react";
import {
  KeyboardAvoidingView,
  ScrollView,
  Platform,
  View,
  TextInput,
} from "react-native";
import { Views } from "@styles";

interface Props {
  children: React.ReactNode | React.ReactNode[];
  scrollEnabled?: boolean;
}

const KeyboardAvoidingWithScrollView: React.FC<Props> = ({
  children,
  scrollEnabled,
}: Props) => {
  return (
    <KeyboardAvoidingView
      style={Views.fill}
      contentContainerStyle={{ backgroundColor: "#ffffff" }}
      behavior="position"
      keyboardVerticalOffset={Platform.OS == "ios" ? 0 : -150}
    >
      <ScrollView
        scrollEnabled={scrollEnabled}
        style={Views.fill}
        contentContainerStyle={Views.fill}
      >
        {children}
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

KeyboardAvoidingWithScrollView.defaultProps = {
  scrollEnabled: true,
};

export default KeyboardAvoidingWithScrollView;
