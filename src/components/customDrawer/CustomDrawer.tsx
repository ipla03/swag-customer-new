import { DrawerNavigationHelpers } from '@react-navigation/drawer/lib/typescript/src/types';
import { Colors } from '@styles';
import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import FirstSectionCard from './FirstSectionCard';
import SectionCard from './SectionCard';
import { translate } from '@utils/translation/translations';

interface Props {
  navigation: DrawerNavigationHelpers;
}

function CustomHeader({ navigation }: Props) {
  return (
    <View style={styles.container}>
      <SectionCard
        color="#27B093"
        title={translate('drawerMonitoring')}
        titleColor="#ffffff"
        onPress={() => navigation.navigate('MachinesMonitoring')}
      />
      <FirstSectionCard
        color="#ffffff"
        title={translate('drawerHome')}
        titleColor={Colors.primary}
        onPress={() => navigation.navigate('Home')}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column-reverse',
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
    width: '100%',
    overflow: 'hidden',
  },
});

export default CustomHeader;
