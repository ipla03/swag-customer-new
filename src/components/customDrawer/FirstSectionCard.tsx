import Constants from "expo-constants";
import React from "react";
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  Platform,
  TouchableOpacity,
} from "react-native";
import Svg, { Polygon } from "react-native-svg";
import { Typography, Views } from "@styles";
import DeviceInfo from "react-native-device-info";
import { normalizeText } from "@utils/textUtils";

const SMALL_DEVICE = Dimensions.get("window").height < 600;

const STATUS_BAR_HEIGHT = Constants.statusBarHeight;
const WIDTH = Dimensions.get("screen").width;
const SECTION_HEIGHT = (SMALL_DEVICE ? 120 : 140) + STATUS_BAR_HEIGHT;

interface Props {
  color: string;
  title?: string;
  titleColor?: string;
  onPress?: () => void;
}

function FirstSectionCard({ color, title, titleColor, onPress }: Props) {
  return (
    <View style={styles.container}>
      <View style={styles.svgContainer}>
        <Svg style={Views.fill}>
          <Polygon
            points={`
          0,0
          ${WIDTH},0
          ${WIDTH},${SECTION_HEIGHT - 20}
          0,${SECTION_HEIGHT}
          `}
            fill={color}
          />
        </Svg>
      </View>
      <TouchableOpacity style={Views.fill} onPress={onPress}>
        <View style={styles.content}>
          <Text style={styles.versionText}>v{DeviceInfo.getVersion()}</Text>
          <Text style={[styles.title, { color: titleColor }]}>{title}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "flex-start",
    justifyContent: "flex-start",
    width: "100%",
    height: SECTION_HEIGHT - 15,
    paddingTop: STATUS_BAR_HEIGHT,
  },
  svgContainer: {
    height: SECTION_HEIGHT,
    width: WIDTH,
    position: "absolute",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.2,
    shadowRadius: 2.65,
  },
  content: {
    height: "100%",
    width: "100%",
    justifyContent: "center",
    alignItems: "flex-start",
    paddingLeft: "15%",
  },
  title: {
    ...Typography.title,
    fontSize: normalizeText(20),
    color: "#ffffff",
  },
  versionText: {
    ...Typography.secondary,
    position: "absolute",
    left: "18%",
    top: 0,
  },
});

export default FirstSectionCard;
