import React from "react";
import Constants from "expo-constants";
import { View, StyleSheet, Dimensions, Image, Platform } from "react-native";
import { Colors } from "@styles";
import Svg, { Polygon } from "react-native-svg";

const SMALL_DEVICE = Dimensions.get("window").height < 600;

const WIDTH = Dimensions.get("window").width;
const STATUS_BAR_HEIGHT = Constants.statusBarHeight;
const HEADER_HEIGHT = (SMALL_DEVICE ? 75 : 100) + STATUS_BAR_HEIGHT;
const HEADER_LOWER_SIDE_DIFFERENCE = 20;
const OVERFLOW_HEIGHT = Dimensions.get("window").height;

interface Props {
  leftButton?: React.ReactNode;
  rightButton?: React.ReactNode;
}

const renderSvgHeader = () => {
  const points = `
  0,0,
  ${WIDTH},0 ${WIDTH},
  ${HEADER_HEIGHT - HEADER_LOWER_SIDE_DIFFERENCE + OVERFLOW_HEIGHT} 0,
  ${HEADER_HEIGHT + OVERFLOW_HEIGHT}`;

  if (Platform.OS == "android") {
    return (
      <View
        style={{
          width: "100%",
          ...styles.headerShadow,
          position: "absolute",
          top: -OVERFLOW_HEIGHT,
        }}
      >
        <Svg width="100%" height={HEADER_HEIGHT + OVERFLOW_HEIGHT}>
          <Polygon points={points} fill={Colors.primary} />
        </Svg>
      </View>
    );
  }

  return (
    <Svg
      width="100%"
      height={HEADER_HEIGHT + OVERFLOW_HEIGHT}
      style={{
        position: "absolute",
        top: -OVERFLOW_HEIGHT,
        ...styles.headerShadow,
      }}
    >
      <Polygon points={points} fill={Colors.primary} />
    </Svg>
  );
};

function Header({ leftButton, rightButton }: Props) {
  return (
    <View style={styles.container}>
      {renderSvgHeader()}
      <View style={styles.contentContainer}>
        <View style={styles.sideElementContainer}>{leftButton}</View>
        <View style={styles.titleContainer}>
          <Image
            style={styles.logo}
            source={require("@assets/images/logo_swag.png")}
            resizeMode="contain"
          />
        </View>
        <View style={styles.sideElementContainer}>{rightButton}</View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: WIDTH,
    height: HEADER_HEIGHT,
    alignItems: "center",
    justifyContent: "center",
    zIndex: 200,
  },
  contentContainer: {
    flex: 1,
    width: "100%",
    marginTop: STATUS_BAR_HEIGHT,
    marginBottom: HEADER_LOWER_SIDE_DIFFERENCE,
    flexDirection: "row",
  },
  sideElementContainer: {
    flex: 1.25,
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  titleContainer: {
    flex: 3,
    height: "100%",
    alignItems: "center",
  },
  logo: {
    width: 175,
    height: "100%",
  },
  headerShadow: {
    shadowOpacity: Platform.OS == "ios" ? 1 : 0.8,
    shadowRadius: Platform.OS == "ios" ? 10 : 5,
    shadowColor: "black",
  },
});

export default Header;
