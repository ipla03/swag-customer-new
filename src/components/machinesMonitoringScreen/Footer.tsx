import { Colors, Typography, Views } from '@styles';
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  ScrollView,
} from 'react-native';
import Svg, { Polygon } from 'react-native-svg';
import { normalizeText } from '@utils/textUtils';
import { translate } from '@utils/translation/translations';

const HEIGHT = Dimensions.get('screen').height;
const WIDTH = Dimensions.get('screen').width;
const SMALL_DEVICE = Dimensions.get('window').height < 600;

function Footer() {
  const renderBackground = () => {
    return (
      <Svg style={styles.svgContainer}>
        <Polygon
          points={`
      0,20
      ${WIDTH},0
      ${WIDTH},20
      `}
          fill="#fff"
        />
      </Svg>
    );
  };

  return (
    <View style={styles.container}>
      <View
        style={[
          StyleSheet.absoluteFill,
          { backgroundColor: '#fff', marginTop: 20, height: HEIGHT * 2 },
        ]}
      />
      <View style={[Views.fill, styles.shadow]}>
        {renderBackground()}
        <View style={styles.content}>
          <View style={styles.imageContainer}>
            <Image
              style={styles.image}
              source={require('@assets/images/machinesMonitoringScreen_image.png')}
              resizeMode="contain"
            />
          </View>
          <View style={styles.textContainer}>
            <View style={[Views.center, { flex: 1, maxHeight: 180 }]}>
              <Text style={styles.title}>
                {translate('monitoringScreenTitle')}
              </Text>
              <Text style={styles.subtitle}>
                {translate('monitoringScreenDescription')}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
  },
  content: {
    height: SMALL_DEVICE ? 300 : 600,
    width: '100%',
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingVertical: SMALL_DEVICE ? '3%' : '10%',
  },
  svgContainer: {
    width: '100%',
    height: 20,
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: -5,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3.84,
  },
  imageContainer: {
    flex: 1,
    width: '100%',
    paddingHorizontal: '10%',
  },
  image: {
    width: '100%',
    height: '100%',
  },
  textContainer: {
    width: '100%',
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: '10%',
  },
  title: {
    ...Typography.title,
    fontSize: normalizeText(16),
    textAlign: 'center',
    marginBottom: 15,
  },
  subtitle: {
    ...Typography.subtitle,
    fontSize: normalizeText(12),
    textAlign: 'center',
    color: Typography.secondary.color,
    marginHorizontal: 20,
  },
});

export default Footer;
