import { Typography, Views } from "@styles";
import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { TextInput } from "react-native-gesture-handler";
import AnteDesign from "react-native-vector-icons/AntDesign";
import Entypo from "react-native-vector-icons/Entypo";

interface Props {
  setSearch: (search: string) => void;
  value: string;
}

function Search({ setSearch, value }: Props) {
  return (
    <View style={styles.container}>
      <View style={[Views.fill, { flexDirection: "row" }]}>
        <View style={styles.iconContainer}>
          <AnteDesign name="search1" size={24} color="#c8d6e5" />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            style={[Views.fill, styles.textInput]}
            value={value}
            onChangeText={setSearch}
          />
        </View>
        <TouchableOpacity onPress={() => setSearch("")}>
          <View style={styles.iconContainer}>
            {value.length > 0 && (
              <Entypo name="cross" size={24} color="#c8d6e5" />
            )}
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: 55,
    width: "80%",
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 100,
    marginVertical: 10,
    borderColor: "#c8d6e5",
    borderWidth: 1,
    paddingLeft: 5,
    paddingRight: 5,
  },
  iconContainer: {
    height: "100%",
    aspectRatio: 1,
    ...(Views.center as object),
  },
  inputContainer: {
    flex: 1,
    height: "100%",
  },
  textInput: {
    ...Typography.subtitle,
  },
});

export default Search;
