import { Colors, Typography } from '@styles';
import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import MachineCardBase from './MachineCardBase';
import { normalizeText } from '@utils/textUtils';
import { fraction } from 'mathjs';
import 'intl';
import 'intl/locale-data/jsonp/it';
import { translate } from '@utils/translation/translations';
import Formatter from '@utils/btcFormatter';

//Not started icons
import DeliveredIcon from '@assets/images/machineNotStartedScreen/delivered_icon.svg';
import ReadyForDeliveryIcon from '@assets/images/machineNotStartedScreen/readyForDelivery_icon.svg';
import SettingUpIcon from '@assets/images/machineNotStartedScreen/settingUp_icon.svg';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

interface Props {
  machine: Machine;
  openMachineInfoScreen: (machine: Machine) => void;
}

function MachineCard({ machine, openMachineInfoScreen }: Props) {
  const expDate = new Date(machine.miningEndDate);
  const startDate = new Date(machine.miningStartDate);

  const f = fraction(machine.fraction) as math.Fraction;

  const isMachineStarted = machine.status === 'PRODUCING';

  function renderStartedInfo() {
    return (
      <>
        <Text style={styles.productionText}>
          {translate('production')} {Formatter.format(machine.production)}{' '}
          <Text style={{ fontSize: normalizeText(9) }}>BTC</Text>
        </Text>
        <Text style={styles.expirationText}>
          {translate('rentEnd')} {expDate.getDate()}/{expDate.getMonth() + 1}/
          {expDate.getFullYear()}
        </Text>
      </>
    );
  }

  function renderNotStartedInfo() {
    return (
      <>
        <Text
          style={[
            styles.productionText,
            { color: Typography.secondary.color, flex: 0.5 },
          ]}>
          {translate('production')} {Formatter.format(0)}{' '}
          <Text style={{ fontSize: normalizeText(9) }}>BTC</Text>
        </Text>
        <Text style={styles.expirationText}>
          {translate('productionStart')} {startDate.getDate()}/
          {startDate.getMonth() + 1}/{startDate.getFullYear()}
        </Text>
      </>
    );
  }

  function renderCorrectInfo() {
    return isMachineStarted ? renderStartedInfo() : renderNotStartedInfo();
  }

  function renderStatusIcon() {
    switch (machine.status) {
      case 'READY_FOR_SHIPPING':
        return <ReadyForDeliveryIcon height="100%" width="100%" />;
      case 'SHIPPED':
        return <DeliveredIcon height="100%" width="100%" />;
      case 'SETTING_UP':
        return <SettingUpIcon height="90%" width="90%" />;
    }
  }

  function renderNotStartedIcon() {
    return (
      <View style={styles.notStartedIconContainer}>{renderStatusIcon()}</View>
    );
  }

  function renderCorrectIcon() {
    return isMachineStarted ? (
      <Image
        style={{ height: 50, width: 50 }}
        source={require('@assets/images/machineStartedCardIcon.gif')}
      />
    ) : (
      renderNotStartedIcon()
    );
  }

  return (
    <TouchableOpacity
      style={{ width: '80%' }}
      onPress={() => openMachineInfoScreen(machine)}>
      <MachineCardBase
        style={{ marginVertical: 10 }}
        color="#fff"
        cardHeight={160}>
        <View style={styles.container}>
          <View style={styles.content}>
            <Text style={styles.nameText}>
              {machine.name} {f.n}
              {f.d !== 1 && `/${f.d}`}
            </Text>
            <Text style={styles.serialNumberText}>{machine.serialNumber}</Text>
            <Text style={styles.machineTypeText}>
              {machine.machineType} {machine.hashRate} TH
            </Text>
            {renderCorrectInfo()}
          </View>
          <View style={styles.iconContainer}>{renderCorrectIcon()}</View>
        </View>
      </MachineCardBase>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 30,
    flexDirection: 'row',
    paddingVertical: 10,
  },
  content: {
    flex: 1,
    height: '100%',
    paddingBottom: '2%',
    paddingTop: '5%',
  },
  iconContainer: {
    height: '100%',
    paddingRight: '5%',
    justifyContent: 'center',
  },
  nameText: {
    ...Typography.title,
    color: Typography.secondary.color,
    fontSize: normalizeText(16),
  },
  productionText: {
    ...Typography.secondary,
    fontSize: wp(3.85),
    lineHeight: wp(3.85),
    color: '#27B093',
    fontFamily: 'Archivo-regular',
  },
  expirationText: {
    ...Typography.secondary,
    fontSize: normalizeText(10),
    fontFamily: 'Archivo-regular',
    color: Colors.primary,
  },
  notStartedIconContainer: {
    height: 50,
    width: 50,
    borderRadius: 30,
    backgroundColor: '#27B093',
    padding: 13,
    alignItems: 'center',
    justifyContent: 'center',
  },
  serialNumberText: {
    ...Typography.secondary,
    flex: 0.7,
  },
  machineTypeText: {
    ...Typography.secondary,
  },
});

export default MachineCard;
