import { Typography } from '@styles';
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Linking,
  Dimensions,
  StyleProp,
  ViewStyle,
} from 'react-native';
import MachineCardBase from './MachineCardBase';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { normalizeText } from '@utils/textUtils';
import { RootState } from '@redux/store';
import { connect, ConnectedProps } from 'react-redux';
import { translate } from '@utils/translation/translations';

const SMALL_DEVICE = Dimensions.get('window').height < 600;

//#region Redux
interface OwnProps {
  style?: StyleProp<ViewStyle>;
}

const mapStateToProps = (state: RootState, ownProps: OwnProps) => ({
  link: state.appInfo.configuration.data?.addMachineLink,
  ...ownProps,
});

const connector = connect(mapStateToProps);

type reduxProps = ConnectedProps<typeof connector>;

type Props = reduxProps;
//#endregion

function AddMachineCard({ link, style }: Props) {
  return (
    <View style={{ width: '80%' }}>
      <MachineCardBase
        style={[{ marginVertical: SMALL_DEVICE ? 0 : 10 }, style]}
        color="#27B093">
        <View style={styles.container}>
          <View style={styles.content}>
            <Text style={styles.nameText}>{translate('newMachineCard')}</Text>
          </View>
          <View style={styles.iconContainer}>
            <TouchableOpacity onPress={() => link && Linking.openURL(link)}>
              <Ionicons
                name="ios-add-circle"
                size={normalizeText(44)}
                color="#fff"
              />
            </TouchableOpacity>
          </View>
        </View>
      </MachineCardBase>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 30,
    flexDirection: 'row',
    paddingVertical: 10,
  },
  content: {
    flex: 1,
    height: '100%',
    paddingTop: '8%',
    paddingBottom: '4%',
    justifyContent: 'center',
  },
  iconContainer: {
    height: '100%',
    paddingRight: '5%',
    justifyContent: 'center',
  },
  nameText: {
    ...Typography.title,
    color: '#fff',
    fontSize: normalizeText(16),
  },
});

export default connector(AddMachineCard);
