import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  StyleProp,
  ViewStyle,
} from 'react-native';
import Svg, { Polygon } from 'react-native-svg';
import { Views } from '@styles';

const WIDTH = Dimensions.get('screen').width;
const CARD_HEIGHT = 135;

interface Props {
  color: string;
  children?: React.ReactNode | React.ReactNode[];
  style?: StyleProp<ViewStyle>;
  cardHeight?: number;
}

function MachineCardBase({ color, children, style, cardHeight }: Props) {
  const height = cardHeight || CARD_HEIGHT;
  return (
    <View style={[styles.container, style, { height: height }]}>
      <View style={[Views.fillAndCenter, styles.headerShadow]}>
        <View style={[Views.fillAndCenter, { overflow: 'hidden' }]}>
          <Svg style={[styles.svgContainer, { height: height }]}>
            <Polygon
              points={`
        0,20
        ${WIDTH}, 0
        ${WIDTH},${height - 20}
        0, ${height}
        `}
              fill={color}
            />
          </Svg>
        </View>
      </View>
      <View style={styles.content}>{children}</View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    ...(Views.fill as object),
    paddingVertical: 10,
  },
  svgContainer: {
    width: WIDTH,
  },
  headerShadow: {
    position: 'absolute',
    shadowOpacity: 0.15,
    shadowRadius: 2,
    shadowOffset: { width: 1, height: 2 },
    shadowColor: '#000',
  },
});

export default MachineCardBase;
