import React, { useEffect, useState } from "react";
import { View, StyleSheet, Dimensions, Platform } from "react-native";
import { RootState } from "@redux/store";
import { connect, ConnectedProps } from "react-redux";
import Animated, {
  runOnJS,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from "react-native-reanimated";
import Constants from "expo-constants";

import LoginStack from "@navigators/LoginStack";
import AppRootStack from "@navigators/AppRootStack";

const STATUS_BAR_HEIGHT = Constants.statusBarHeight;
const HEIGHT =
  Dimensions.get("window").height +
  (Platform.OS === "ios" ? 0 : STATUS_BAR_HEIGHT);

//#region Redux
const mapStateToProps = (state: RootState) => ({
  isLogged: state.user.isLogged,
});

const connector = connect(mapStateToProps);

type reduxProps = ConnectedProps<typeof connector>;

type Props = reduxProps;
//#endregion

function AuthSwitcher({ isLogged }: Props) {
  const [showLogin, setShowLogin] = useState(true);
  const [showContent, setShowContent] = useState(false);
  const transY = useSharedValue(0);

  const appContentAnimatedStyle = useAnimatedStyle(() => ({
    transform: [{ translateY: transY.value }],
  }));

  useEffect(() => {
    if (isLogged) {
      setShowContent(true);
      transY.value = withTiming(-HEIGHT, {}, () =>
        runOnJS(setShowLogin)(false)
      );
    } else {
      setShowLogin(true);
      transY.value = withTiming(0, {}, () => runOnJS(setShowContent)(false));
    }
  }, [isLogged]);

  return (
    <View style={styles.container}>
      <View style={styles.screenContainer}>{showLogin && <LoginStack />}</View>
      <Animated.View style={[styles.screenContainer, appContentAnimatedStyle]}>
        {showContent && <AppRootStack />}
      </Animated.View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: HEIGHT * 2,
    alignItems: "center",
    justifyContent: "flex-start",
  },
  screenContainer: {
    height: HEIGHT,
    width: "100%",
    backgroundColor: "#ffffff",
  },
});

export default connector(AuthSwitcher);
