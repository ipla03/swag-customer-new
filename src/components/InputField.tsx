import React, { useEffect, useRef } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  StyleProp,
  ViewStyle,
} from 'react-native';
import { Colors } from '@styles';

interface Props {
  label: string;
  placeHolder: string;
  value: string;
  setValue: (value: string) => void;
  secureTextEntry?: boolean;
  style?: StyleProp<ViewStyle>;
}

function InputField({
  style,
  value,
  setValue,
  label,
  placeHolder,
  secureTextEntry,
}: Props) {
  const ref = useRef<TextInput>(null);

  //Workaround for react native bug causing the font family to not be applied on android
  useEffect(() => {
    ref.current?.setNativeProps({
      style: { fontFamily: styles.textInput.fontFamily },
    });
  }, []);

  return (
    <View style={[{ width: '100%', marginBottom: 20 }, style]}>
      <Text style={styles.titleText}>{label}</Text>
      <TextInput
        ref={ref}
        style={styles.textInput}
        placeholder={placeHolder}
        placeholderTextColor="#838383"
        value={value}
        onChangeText={setValue}
        secureTextEntry={secureTextEntry || false}
      />
      <View style={styles.line} />
    </View>
  );
}

const styles = StyleSheet.create({
  titleText: {
    fontFamily: 'Archivo-SemiBold',
    fontSize: 15,
    color: Colors.primary,
  },
  textInput: {
    fontFamily: 'Archivo-Medium',
    lineHeight: 16,
    height: 20,
    fontSize: 12,
    color: '#222f3e',
    paddingVertical: 0,
    paddingHorizontal: 0,
    marginVertical: 1,
  },
  line: {
    height: 1,
    width: '100%',
    backgroundColor: '#828282',
    marginVertical: 5,
  },
});

export default InputField;
