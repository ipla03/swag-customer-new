import React, { Children } from "react";
import {
  TouchableOpacity,
  Text,
  StyleSheet,
  ActivityIndicator,
} from "react-native";

interface Props {
  height: number | string;
  width: number | string;
  color: string;
  onPress: () => void;
  label: string;
  loading?: boolean;
}

function Button({ height, width, color, label, onPress, loading }: Props) {
  return (
    <TouchableOpacity
      disabled={loading}
      onPress={onPress}
      style={{
        width: width,
        height: height,
        backgroundColor: color,
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      {loading ? (
        <ActivityIndicator color="#ffffff" />
      ) : (
        <Text
          style={{
            color: "#FFFFFF",
            fontFamily: "Archivo-SemiBold",
            fontSize: 20,
          }}
        >
          {label}
        </Text>
      )}
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});

export default Button;
