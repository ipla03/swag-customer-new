import React, { useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Drone from '@assets/images/machineNotStartedScreen/drone_image.svg';
import Animated, {
  Easing,
  useAnimatedStyle,
  useSharedValue,
  withRepeat,
  withSequence,
  withTiming,
} from 'react-native-reanimated';
import {} from 'react-native-redash';

function AnimatedDrone() {
  const transY = useSharedValue(0);
  const animStyle = useAnimatedStyle(() => ({
    transform: [{ translateY: transY.value }],
  }));

  useEffect(() => {
    transY.value = withSequence(
      withTiming(10, {
        duration: 1000,
        easing: Easing.inOut(Easing.ease),
      }),
      withRepeat(
        withTiming(-10, {
          duration: 1000,
          easing: Easing.inOut(Easing.ease),
        }),
        -1,
        true,
      ),
    );
  }, []);

  return (
    <View style={styles.container}>
      <Animated.View style={animStyle}>
        <Drone />
      </Animated.View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default AnimatedDrone;
