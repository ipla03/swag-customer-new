import React, { useEffect } from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import ReadyForDeliveryBack from '@assets/images/machineNotStartedScreen/readyForDeliveryBack_image.svg';
import ReadyForDeliveryFront from '@assets/images/machineNotStartedScreen/readyForDeliveryFront_image.svg';
import Animated, {
  interpolate,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';

const SMALL_DEVICE = Dimensions.get('window').height < 600;

function AnimatedDrone() {
  const animatedValue = useSharedValue(0);

  const frontAnimStyle = useAnimatedStyle(() => ({
    position: 'absolute',
    transform: [
      { translateY: 30 },
      {
        scale: interpolate(
          animatedValue.value,
          [0, 1],
          [0, SMALL_DEVICE ? 1 : 1.25],
        ),
      },
    ],
    opacity: animatedValue.value,
  }));

  const backAnimStyle = useAnimatedStyle(() => ({
    position: 'absolute',
    transform: [
      { translateY: interpolate(animatedValue.value, [0, 1], [-30, 0]) },
      { scale: SMALL_DEVICE ? 1 : 1.25 },
    ],
    opacity: animatedValue.value,
  }));

  useEffect(() => {
    animatedValue.value = withTiming(1, {
      duration: 1500,
    });
  }, []);

  return (
    <View style={styles.container}>
      <Animated.View style={backAnimStyle}>
        <ReadyForDeliveryBack />
      </Animated.View>
      <Animated.View style={frontAnimStyle}>
        <ReadyForDeliveryFront />
      </Animated.View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    width: 50,
  },
});

export default AnimatedDrone;
