import React, { useEffect } from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import SettingsBackground from '@assets/images/machineNotStartedScreen/settingsBackground_image.svg';
import SettingsPeople from '@assets/images/machineNotStartedScreen/settingsPeople_image.svg';
import Animated, {
  interpolate,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';

const SMALL_DEVICE = Dimensions.get('window').height < 600;

function AnimatedSettings() {
  const animatedValue = useSharedValue(0);

  const frontAnimStyle = useAnimatedStyle(() => ({
    position: 'absolute',
    transform: [
      { translateY: 30 },
      {
        scale: interpolate(
          animatedValue.value,
          [0, 1],
          [0, SMALL_DEVICE ? 1 : 1.25],
        ),
      },
    ],
    opacity: animatedValue.value,
  }));

  const backAnimStyle = useAnimatedStyle(() => ({
    position: 'absolute',
    transform: [
      { translateY: interpolate(animatedValue.value, [0, 1], [-30, 0]) },
      { scale: SMALL_DEVICE ? 1 : 1.25 },
    ],
    opacity: animatedValue.value,
  }));

  useEffect(() => {
    animatedValue.value = withTiming(1, {
      duration: 1500,
    });
  }, []);

  return (
    <View style={styles.container}>
      <Animated.View style={backAnimStyle}>
        <SettingsBackground />
      </Animated.View>
      <Animated.View style={frontAnimStyle}>
        <SettingsPeople />
      </Animated.View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    width: 50,
  },
});

export default AnimatedSettings;
