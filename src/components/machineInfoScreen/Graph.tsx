import * as React from 'react';
import {
  Easing,
  TextInput,
  Animated,
  Text,
  View,
  StyleSheet,
  Dimensions,
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import Svg, { G, Circle } from 'react-native-svg';
import { Colors } from '@styles';
import { normalizeText } from '@utils/textUtils';
import { translate } from '@utils/translation/translations';

const AnimatedTextInput = Animated.createAnimatedComponent(TextInput);
const WIDTH = Dimensions.get('window').width;

interface Props {
  value: number;
  max: number;
}

export default function Donut({ value, max }: Props) {
  const radius = 110 * (WIDTH / 430);
  const strokeWidth = 20;

  const animated = React.useRef(new Animated.Value(0)).current;
  const circleRef = React.useRef();
  const inputRef = React.useRef();
  const circumference = 2 * Math.PI * radius;
  const halfCircle = radius + strokeWidth;

  const animation = (toValue: number) => {
    return Animated.timing(animated, {
      toValue,
      duration: 500,
      useNativeDriver: true,
      easing: Easing.out(Easing.ease),
    }).start();
  };

  useFocusEffect(() => {
    animation(value);
    animated.addListener(v => {
      const maxPerc = (100 * v.value) / max;
      const strokeDashoffset = circumference - (circumference * maxPerc) / 100;
      if (inputRef?.current) {
        inputRef.current.setNativeProps({
          text: `${Math.round(v.value)}`,
        });
      }
      if (circleRef?.current) {
        circleRef.current.setNativeProps({
          strokeDashoffset,
        });
      }
    });

    return () => {
      animated.removeAllListeners();
      animated.setValue(0);
      circleRef.current.setNativeProps({
        strokeDashoffset: circumference,
      });
      inputRef.current.setNativeProps({
        text: '0',
      });
    };
  });

  return (
    <View style={{ width: radius * 2, height: radius * 2 }}>
      <Svg
        height={radius * 2}
        width={radius * 2}
        viewBox={`0 0 ${halfCircle * 2} ${halfCircle * 2}`}>
        <G rotation="-90" origin={`${halfCircle}, ${halfCircle}`}>
          <Circle
            cx="50%"
            cy="50%"
            r={radius}
            fill="transparent"
            stroke={Colors.primary}
            strokeWidth={strokeWidth}
            strokeLinejoin="round"
            strokeOpacity="1"
          />
          <Circle
            ref={circleRef}
            cx="50%"
            cy="50%"
            r={radius}
            fill="transparent"
            stroke="#f3f3f3"
            strokeWidth={strokeWidth}
            strokeLinecap="round"
            strokeDashoffset={circumference}
            strokeDasharray={circumference}
          />
        </G>
      </Svg>
      <View style={[StyleSheet.absoluteFill, { justifyContent: 'center' }]}>
        <AnimatedTextInput
          ref={inputRef}
          underlineColorAndroid="transparent"
          editable={false}
          defaultValue="0"
          style={[
            {
              fontSize: normalizeText(40),
              color: '#fff',
              padding: 0,
            },
            styles.text,
          ]}
        />
        <Text style={styles.descriptionText}>{translate('remainingDays')}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Archivo-Regular',
    textAlign: 'center',
    lineHeight: normalizeText(40),
  },
  descriptionText: {
    fontFamily: 'Archivo-Regular',
    textAlign: 'center',
    textTransform: 'uppercase',
    color: '#fff',
    fontSize: normalizeText(12),
    paddingHorizontal: '20%',
    letterSpacing: 1,
    lineHeight: normalizeText(12),
  },
});
