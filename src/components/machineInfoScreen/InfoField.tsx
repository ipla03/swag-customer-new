import { Typography } from '@styles';
import React from 'react';
import {
  StyleProp,
  StyleSheet,
  View,
  ViewProps,
  Text,
  Image,
} from 'react-native';

interface InfoFieldProps {
  title: string;
  value: string | number;
  style?: StyleProp<ViewProps>;
}

function InfoField({ title, value, style }: InfoFieldProps) {
  return (
    <View style={[styles.container, style]}>
      <Text style={styles.infoFieldTitle}>{title}</Text>
      <View style={[{ flexDirection: 'row', alignItems: 'center' }]}>
        <Text
          style={styles.infoFieldText}
          adjustsFontSizeToFit
          numberOfLines={1}>
          {value}
        </Text>
        <Image
          style={{ height: 17, width: 17 }}
          source={require('@assets/images/btc_icon.png')}
          resizeMode="contain"
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginVertical: 5,
  },
  infoFieldTitle: {
    ...Typography.subtitle,
    textTransform: 'uppercase',
  },
  infoFieldText: {
    ...Typography.subtitle,
    color: '#fff',
    fontSize: 50,
    marginRight: 3,
  },
});

export default InfoField;
