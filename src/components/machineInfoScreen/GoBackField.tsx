import { Typography, Views } from '@styles';
import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  StyleProp,
  ViewStyle,
  TouchableOpacity,
  Text,
} from 'react-native';
import Svg, { Polygon } from 'react-native-svg';
import BackButton from '@assets/images/back_button.svg';
import { normalizeText } from '@utils/textUtils';
import { translate } from '@utils/translation/translations';

const BACKGROUND_HEIGHT = 110;
const WIDTH = Dimensions.get('screen').width;

interface Props {
  style?: StyleProp<ViewStyle>;
  zIndex?: number;
  goBack: () => void;
}

function GoBackField({ goBack, style, zIndex }: Props) {
  function renderBackGround() {
    return (
      <Svg style={styles.svgContainer}>
        <Polygon
          points={`0,0 ${WIDTH},0 ${WIDTH}, ${
            BACKGROUND_HEIGHT - 20
          } 0,${BACKGROUND_HEIGHT}`}
          fill="#F2F2F2"
        />
      </Svg>
    );
  }

  return (
    <View style={[styles.container, { zIndex: zIndex }]}>
      {renderBackGround()}
      <View style={[styles.content, style]}>
        <TouchableOpacity style={styles.touchableArea} onPress={goBack}>
          <BackButton
            style={{ marginRight: 10 }}
            width={normalizeText(15)}
            height={normalizeText(15)}
          />
          <Text
            style={[
              Typography.secondary,
              {
                fontSize: normalizeText(14),
                lineHeight: normalizeText(14),
                fontFamily: 'Archivo-Regular',
              },
            ]}>
            {translate('machineList')}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: BACKGROUND_HEIGHT - 20,
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: WIDTH,
    paddingBottom: 20,
  },
  content: {
    ...(Views.fill as object),
  },
  svgContainer: {
    width: WIDTH,
    height: BACKGROUND_HEIGHT,
    position: 'absolute',
    top: -20,
  },
  touchableArea: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: '5%',
  },
});

export default GoBackField;
