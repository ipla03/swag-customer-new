import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Platform,
  SafeAreaView,
} from 'react-native';
import { Colors, Views } from '@styles';
import Svg, { Polygon } from 'react-native-svg';
import Constants from 'expo-constants';
import DateTime from '@components/DateTime';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { translate } from '@utils/translation/translations';

const STATUS_BAR_HEIGHT = Constants.statusBarHeight;
const SMALL_DEVICE = Dimensions.get('window').height < 600;
const WIDTH = Dimensions.get('window').width;
const FOOTER_HEIGHT = (SMALL_DEVICE ? 125 : 150) + STATUS_BAR_HEIGHT;
const FOOTER_LOWER_SIDE_DIFFERENCE = 20;
const OVERFLOW_HEIGHT = Dimensions.get('window').height;

interface Props {
  startDate: Date;
  endDate: Date;
}

function Footer({ startDate, endDate }: Props) {
  const safeAreaInsets = useSafeAreaInsets();

  const points = `
    0,${FOOTER_LOWER_SIDE_DIFFERENCE},
    ${WIDTH},0 ${WIDTH},
    ${FOOTER_HEIGHT + OVERFLOW_HEIGHT} 0,
    ${FOOTER_HEIGHT + OVERFLOW_HEIGHT}`;

  return (
    <View style={[styles.container, { paddingBottom: safeAreaInsets.bottom }]}>
      <Svg
        width="100%"
        height={FOOTER_HEIGHT + OVERFLOW_HEIGHT}
        style={{
          position: 'absolute',
          bottom: -OVERFLOW_HEIGHT,
          ...styles.headerShadow,
        }}>
        <Polygon points={points} fill={Colors.primary} />
      </Svg>
      <View style={styles.content}>
        <DateTime title={translate('activationDate')} date={startDate} />
        <DateTime title={translate('productionEnd')} date={endDate} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: WIDTH,
    height: FOOTER_HEIGHT,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 20,
  },
  content: {
    ...(Views.fillAndCenter as object),
    flexDirection: 'row',
  },
  headerShadow: {
    shadowOpacity: Platform.OS == 'ios' ? 0.4 : 0.7,
    shadowRadius: Platform.OS == 'ios' ? 10 : 5,
    shadowColor: 'black',
  },
});

export default Footer;
