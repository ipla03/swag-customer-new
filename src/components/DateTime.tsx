import { Colors } from "@styles";
import React from "react";
import { Text, View, StyleSheet } from "react-native";

interface DateTimeProps {
  title: string;
  date: Date;
}

function DateTime({ date, title }: DateTimeProps) {
  return (
    <View style={styles.container}>
      <Text style={styles.title} numberOfLines={1} adjustsFontSizeToFit>
        {title}
      </Text>
      <View style={styles.dateContainer}>
        <View style={styles.dateValueRect}>
          <Text style={styles.dateValueText}>
            {date.getDate().toString().padStart(2, "0")}
          </Text>
        </View>
        <View style={styles.dateValueRect}>
          <Text style={styles.dateValueText}>
            {(date.getMonth() + 1).toString().padStart(2, "0")}
          </Text>
        </View>
        <View style={styles.dateValueRect}>
          <Text style={styles.dateValueText}>
            {date.getFullYear().toString().substring(2)}
          </Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    color: "#fff",
    fontFamily: "Archivo-Medium",
    textTransform: "uppercase",
    letterSpacing: 1,
    fontSize: 14,
    marginLeft: 5,
    width: 160,
  },
  dateContainer: {
    height: 50,
    width: 150,
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 20,
  },
  dateValueRect: {
    height: 45,
    width: 45,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
  },
  dateValueText: {
    color: Colors.primary,
    fontFamily: "Roboto-Bold",
    fontSize: 30,
  },
});

export default DateTime;
