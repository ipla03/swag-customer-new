import PushNotification from 'react-native-push-notification';
import messaging from '@react-native-firebase/messaging';
import { registerUser } from '@apis/notifcationApis';

const DEFAULT_CHANNEL_ID = 'DEFAULT_CHANNEL';
const REMOTE_CHANNEL_ID = 'REMOTE_CHANNEL';

const defaultMessaging = messaging();

/**
 * Configure notifications
 */
PushNotification.configure({
  onNotification: function (notification) {
    console.log('REMOTE NOTIFICATION ==>', notification);
  },

  //@ts-ignore
  senderID: '26345674318',
  requestPermissions: true,
});

/**
 * Create a notification channel, required for android
 */
PushNotification.createChannel(
  {
    channelId: DEFAULT_CHANNEL_ID,
    channelName: 'default channel',
    channelDescription: 'Default channel for generic notifications',
    playSound: true,
    soundName: 'default',
    importance: 4,
    vibrate: true,
  },
  created => console.log(`createChannel returned '${created}'`),
);

/**
 * Create notification channel for remote notifications sent by firebase
 */
PushNotification.createChannel(
  {
    channelId: REMOTE_CHANNEL_ID,
    channelName: 'remote channel',
    channelDescription: 'channel used for remote notifications',
    playSound: true,
    soundName: 'default',
    importance: 4,
    vibrate: true,
  },
  created => console.log(`createChannel returned '${created}'`),
);

export async function registerUserOnNotificationModule(
  userId: string,
  jwt: string,
) {
  const firebaseToken = await defaultMessaging.getToken();
  registerUser(userId, firebaseToken, jwt);
}
