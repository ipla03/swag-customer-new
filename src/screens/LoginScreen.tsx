import React, { useContext, useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  StatusBar,
  Dimensions,
  Image,
  Text,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import Header from '@components/Header';
import { Colors, Typography, Views } from '@styles';
import InputField from '@components/InputField';
import Button from '@components/Button';
import KeyboardAvoidingWithScrollView from '@components/KeyboardAvoidingWithScrollView';
import { RootState } from '@redux/store';
import { connect, ConnectedProps } from 'react-redux';
import { loginUser } from '@apis/userLogin';
import DeviceInfo from 'react-native-device-info';
import Constants from 'expo-constants';
import { getAppConfiguration } from '@apis/getAppConfiguration';
import { StackNavigationProp } from '@react-navigation/stack';
import AntDesign from 'react-native-vector-icons/AntDesign';
import * as SecureStore from 'expo-secure-store';
import Config from 'react-native-config';
import * as LocalAuthentication from 'expo-local-authentication';
import { translate } from '@utils/translation/translations';

const SMALL_DEVICE = Dimensions.get('window').height < 600;
const STATUS_BAR_HEIGHT = Constants.statusBarHeight;
const HEADER_HEIGHT = (SMALL_DEVICE ? 75 : 100) + STATUS_BAR_HEIGHT;

//#region Redux
interface OwnProps {
  navigation: StackNavigationProp<LoginStackNavigatorParamList, 'Login'>;
}

const mapStateToProps = (state: RootState, ownProps: OwnProps) => ({
  status: state.loginScreen.status,
  error: state.loginScreen.error,
  appConfiguration: state.appInfo.configuration,
  needUpdate: state.appInfo.needUpdate,
  ...ownProps,
});

const mapDispatchToProps = {
  login: loginUser,
  getAppConfiguration: getAppConfiguration,
};

const connector = connect(mapStateToProps, mapDispatchToProps);

type reduxProps = ConnectedProps<typeof connector>;

type Props = reduxProps;
//#endregion

function LoginScreen({
  status,
  error,
  login,
  appConfiguration,
  getAppConfiguration,
  navigation,
  needUpdate,
}: Props) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  useEffect(() => {
    getAppConfiguration();
  }, []);

  useEffect(() => {
    if (appConfiguration.data?.maintenance?.enabled) {
      navigation.navigate('Maintenance');
    }

    if (appConfiguration.status === 'succeed' && !needUpdate) {
      tryLoginWithBioAuth();
    }
  }, [appConfiguration]);

  useEffect(() => {
    if (needUpdate) {
      navigation.navigate('Update');
    }
  }, [needUpdate]);

  const tryBioAuth = async (): Promise<boolean> => {
    if (!(await LocalAuthentication.hasHardwareAsync())) {
      return false;
    }

    const result = await LocalAuthentication.authenticateAsync({
      promptMessage: 'Authenticate to login',
    });

    return result.success;
  };

  const tryLoginWithBioAuth = async () => {
    const [email, password] = await Promise.all([
      SecureStore.getItemAsync(Config.EMAIL_SECURE_STORE_KEY),
      SecureStore.getItemAsync(Config.PASSWORD_SECURE_STORE_KEY),
    ]);

    if (email && password) {
      if (await tryBioAuth()) {
        login({ email, password });
      }
    }
  };

  const renderAppConfigLoading = () => {
    if (
      appConfiguration.status === 'idle' ||
      appConfiguration.status === 'loading' ||
      appConfiguration.status === 'failed'
    ) {
      return (
        <View
          style={[
            StyleSheet.absoluteFill,
            Views.center,
            {
              backgroundColor: 'rgba(255, 255, 255, 0.8)',
              zIndex: 100,
              top: -20,
              paddingTop: HEADER_HEIGHT,
            },
          ]}>
          {appConfiguration.status === 'failed' ? (
            <>
              <Text style={[Typography.subtitle, { marginBottom: 10 }]}>
                {appConfiguration.error}
              </Text>
              <TouchableOpacity onPress={getAppConfiguration}>
                <AntDesign name="reload1" size={24} color={Colors.primary} />
              </TouchableOpacity>
            </>
          ) : (
            <>
              <ActivityIndicator color={Colors.primary} />
              <Text style={[Typography.subtitle, { marginTop: 10 }]}>
                Collegamento con il server...
              </Text>
            </>
          )}
        </View>
      );
    }
  };

  return (
    <KeyboardAvoidingWithScrollView
      scrollEnabled={appConfiguration.status === 'succeed'}>
      <View style={Views.fill}>
        <Header />
        {renderAppConfigLoading()}
        <View style={styles.content}>
          <View style={styles.imageContainer}>
            <Image
              style={styles.image}
              source={require('@assets/images/login_image.png')}
              resizeMode="contain"
            />
          </View>
          <View style={styles.form}>
            <View style={[Views.fill, Views.center, { maxHeight: 375 }]}>
              <View style={[Views.center, { flex: 1 }]}>
                <Text style={[Typography.title, { marginBottom: 4 }]}>
                  {translate('userLogin')}
                </Text>
                <Text style={[Typography.subtitle, { marginBottom: 4 }]}>
                  {translate('userLoginDescription')}
                </Text>
                <Text style={Typography.error}>{error}</Text>
              </View>
              <View
                style={[
                  Views.center,
                  { width: '100%', flex: SMALL_DEVICE ? 1.7 : 2 },
                ]}>
                <InputField
                  style={{ width: '65%' }}
                  value={username}
                  setValue={setUsername}
                  placeHolder={translate('usernameFieldPlaceholder')}
                  label="USERNAME"
                />
                <InputField
                  style={{ width: '65%' }}
                  value={password}
                  setValue={setPassword}
                  placeHolder={translate('passwordFieldPlaceholder')}
                  label="PASSWORD"
                  secureTextEntry
                />
              </View>
              <View
                style={[
                  Views.center,
                  { width: '100%', flex: SMALL_DEVICE ? 0.7 : 1 },
                ]}>
                <Button
                  height={SMALL_DEVICE ? 50 : 60}
                  width="65%"
                  color={Colors.secondary}
                  label="Login"
                  onPress={() => login({ email: username, password: password })}
                  loading={status === 'loading'}
                />
              </View>
              <View style={[Views.center, { flex: 0.25 }]}>
                <Text style={Typography.secondary}>
                  Swag client @v{DeviceInfo.getVersion()}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </KeyboardAvoidingWithScrollView>
  );
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    maxHeight: 725,
  },
  form: {
    width: '100%',
    flex: 2,
    alignItems: 'center',
    justifyContent: 'space-evenly',
    paddingBottom: SMALL_DEVICE ? 5 : 20,
  },
  imageContainer: {
    width: '100%',
    flex: SMALL_DEVICE ? 0.9 : 1.25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    height: SMALL_DEVICE ? '90%' : '85%',
    width: '100%',
  },
});

export default connector(LoginScreen);
