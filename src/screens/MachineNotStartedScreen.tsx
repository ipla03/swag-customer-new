import HeaderWithMenuAndProfileButton from '@components/HeaderWithMenuAndProfile';
import { DrawerNavigationProp } from '@react-navigation/drawer';
import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootState } from '@redux/store';
import { Typography, Views } from '@styles';
import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
  Text,
  Platform,
} from 'react-native';
import { connect, ConnectedProps } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import GoBackField from '@components/machineInfoScreen/GoBackField';
import Svg, { Polygon } from 'react-native-svg';
import { normalizeText } from '@utils/textUtils';
import { translate } from '@utils/translation/translations';
import { fraction } from 'mathjs';
import Footer from '@components/machineNotStartedScreen/Footer';
import Constants from 'expo-constants';

//Footer icons
import DeliveredIcon from '@assets/images/machineNotStartedScreen/delivered_icon.svg';
import ReadyForDeliveryIcon from '@assets/images/machineNotStartedScreen/readyForDelivery_icon.svg'
import SettingUpIcon from '@assets/images/machineNotStartedScreen/settingUp_icon.svg'

//Animated images
import AnimatedReadyForDelivery from '@components/machineNotStartedScreen/AnimatedReadyForDelivery'
import AnimatedSettings from '@components/machineNotStartedScreen/AnimatedSettings'
import AnimatedDrone from '@components/machineNotStartedScreen/AnimatedDrone'

const SMALL_DEVICE = Dimensions.get('window').height < 600;
const WIDTH = Dimensions.get('screen').width;
const NAME_CONTAINER_HEIGHT = SMALL_DEVICE ? 120 : 140;

//#region Redux
interface OwnProps {
  navigation: DrawerNavigationProp<
    RootDrawerNavigatorParamList,
    'MachinesMonitoring'
  > &
    StackNavigationProp<MachinesMonitoringScreenStack, 'MachineNotStarted'> &
    StackNavigationProp<RootStackNavigatorParamList, 'RootDrawer'>;
  route: RouteProp<MachinesMonitoringScreenStack, 'MachineNotStarted'>;
}

const mapStateToProps = (state: RootState, ownProps: OwnProps) => ({
  profilePicture: state.user.userInfo?.profilePicture,
  ...ownProps,
});

const connector = connect(mapStateToProps);

type reduxProps = ConnectedProps<typeof connector>;

type Props = reduxProps;
//#endregion

function MachineNotStartedScreen({ profilePicture, route, navigation }: Props) {
  const { machine } = route.params;
  const f = fraction(machine.fraction) as math.Fraction;

  const startDate = new Date(machine.miningStartDate)
  function getFormattedStartDate() {
    return `${startDate.getDate()}/${startDate.getMonth() + 1}/${startDate.getFullYear()}`
  }

  function renderBackButton() {
    return (
      <View style={Views.fillAndCenter}>
        <TouchableOpacity onPress={() => navigation.pop()}>
          <Icon name="ios-arrow-back" color="#ffffff" size={30} />
        </TouchableOpacity>
      </View>
    );
  }

  function renderMachineName() {
    return (
      <View style={styles.machineNameContainer}>
        <Svg
          style={{
            width: WIDTH,
            height: NAME_CONTAINER_HEIGHT + 20,
            position: 'absolute',
            top: -20,
          }}>
          <Polygon
            points={`0,20 ${WIDTH},0 ${WIDTH},${NAME_CONTAINER_HEIGHT} 0,${
              NAME_CONTAINER_HEIGHT + 20
            }`}
            fill="#27B093"
          />
        </Svg>
        <View style={styles.nameContainer}>
          <Image
            style={styles.circuits}
            source={require('@assets/images/machineInfoScreenCircuits_image.png')}
            resizeMode="contain"
          />
          <Text style={styles.minerText}>{translate('miner')}</Text>
          <Text style={styles.nameText}>
            {machine.name} {machine.fraction !== 1 && `- ${f.n}/${f.d}`}
          </Text>
        </View>
      </View>
    );
  }

  function renderCorrectImage() {
    switch(machine.status) {
      case "READY_FOR_SHIPPING":
        return <AnimatedReadyForDelivery />
      case "SHIPPED":
        return <AnimatedDrone />
      case "SETTING_UP":
        return <AnimatedSettings />
    }
  }

  function getCorrectStatusText() {
    switch(machine.status) {
      case "READY_FOR_SHIPPING":
        return translate("statusReadyForShipping")
      case "SHIPPED":
        return translate("statusShipped")
      case "SETTING_UP":
        return translate("statusSettingUp")
    }
  }

  function renderCorrectIcon() {
    switch(machine.status) {
      case "READY_FOR_SHIPPING":
        return <ReadyForDeliveryIcon />
      case "SHIPPED":
        return <DeliveredIcon />
      case "SETTING_UP":
        return <SettingUpIcon />
    }
  }

  return (
    <View style={styles.container}>
      <HeaderWithMenuAndProfileButton
        profilePictureUrl={profilePicture}
        openMenu={navigation.openDrawer}
        navigateToProfile={() => navigation.navigate('Profile')}
        alternativeMenu={SMALL_DEVICE ? renderBackButton() : undefined}
      />
      {!SMALL_DEVICE && (
        <GoBackField
          style={styles.backContainer}
          zIndex={99}
          goBack={() => navigation.pop()}
        />
      )}
      {renderMachineName()}
      <View style={[Views.center, { flex: 1 }]}>
        {renderCorrectImage()}
      </View>
      <Footer>
        <View style={styles.machineStatusContainer}>
          <View style={{ flex: 1 }}>
            <Text style={styles.machineStatusTitle}>{translate("machineStatus")}</Text>
            <Text
              style={styles.machineStatusText}
              numberOfLines={1}
              adjustsFontSizeToFit>
              {getCorrectStatusText()}
            </Text>
          </View>
          {renderCorrectIcon()}
        </View>
        <Text 
          style={styles.startDateText}
          numberOfLines={1} >
            {translate("productionStart")} {getFormattedStartDate()} 
          </Text>
      </Footer>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingBottom: Platform.OS === "android" ? Constants.statusBarHeight : 0,
    backgroundColor: "#fff"
  },
  backContainer: {
    alignItems: 'center',
    paddingLeft: '10%',
    flexDirection: 'row',
  },
  machineNameContainer: {
    width: WIDTH,
    height: NAME_CONTAINER_HEIGHT,
  },
  minerText: {
    ...Typography.subtitle,
    textTransform: 'uppercase',
    letterSpacing: 2,
    fontSize: 18,
  },
  nameText: {
    color: '#3DFF39',
    textTransform: 'uppercase',
    fontSize: normalizeText(24),
    fontFamily: 'Archivo-Regular',
  },
  circuits: {
    position: 'absolute',
    width: '100%',
    height: 300,
    right: 0,
    top: '-190%',
  },
  nameContainer: {
    width: '100%',
    flex: 1,
    paddingLeft: '10%',
    paddingTop: SMALL_DEVICE ? '5%' : '10%',
  },
  machineStatusContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '70%',
    alignItems: 'center',
  },
  machineStatusTitle: {
    ...Typography.subtitle,
    color: '#27B093',
    textTransform: 'uppercase',
    letterSpacing: 1.2,
  },
  machineStatusText: {
    ...Typography.subtitle,
    color: '#fff',
    fontSize: 20,
    marginTop: 3,
    width: '90%',
  },
  startDateText: {
    ...Typography.subtitle,
    color: '#fff',
    fontSize: 15,
    marginTop: 10,
    width: '70%',
  }
});

export default connector(MachineNotStartedScreen);
