import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';
import { Typography, Views } from '@styles';
import Icon from 'react-native-vector-icons/Ionicons';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/core';
import Footer from '@components/machineInfoScreen/Footer';
import { normalizeText } from '@utils/textUtils';
import { fraction } from 'mathjs';
import Graph from '@components/machineInfoScreen/Graph';
import InfoField from '@components/machineInfoScreen/InfoField';
import GoBackField from '@components/machineInfoScreen/GoBackField';
import { DrawerNavigationProp } from '@react-navigation/drawer';
import { RootState } from '@redux/store';
import { connect, ConnectedProps } from 'react-redux';
import { translate } from '@utils/translation/translations';
import HeaderWithMenuAndProfileButton from '@components/HeaderWithMenuAndProfile';
import Formatter from '@utils/btcFormatter';
import { isSmallDevice } from '@utils/isSmallDevice';

const SMALL_DEVICE = isSmallDevice();

//#region Redux
interface OwnProps {
  navigation: DrawerNavigationProp<
    RootDrawerNavigatorParamList,
    'MachinesMonitoring'
  > &
    StackNavigationProp<MachinesMonitoringScreenStack, 'Info'> &
    StackNavigationProp<RootStackNavigatorParamList, 'RootDrawer'>;
  route: RouteProp<MachinesMonitoringScreenStack, 'Info'>;
}

const mapStateToProps = (state: RootState, ownProps: OwnProps) => ({
  profilePicture: state.user.userInfo?.profilePicture,
  ...ownProps,
});

const connector = connect(mapStateToProps);

type reduxProps = ConnectedProps<typeof connector>;

type Props = reduxProps;
//#endregion

function MachinesInfoScreen({ navigation, route, profilePicture }: Props) {
  const { machine } = route.params;
  const f = fraction(machine.fraction) as math.Fraction;

  const startDate = new Date(machine.miningStartDate);
  const endDate = new Date(machine.miningEndDate);

  const totalDays = getDaysBetween(startDate, endDate);
  const remainingDays = getDaysBetween(new Date(), endDate);

  function renderBackButton() {
    return (
      <View style={Views.fillAndCenter}>
        <TouchableOpacity onPress={() => navigation.pop()}>
          <Icon name="ios-arrow-back" color="#ffffff" size={30} />
        </TouchableOpacity>
      </View>
    );
  }

  function renderMachineNameContainer() {
    return (
      <View style={styles.nameContainer}>
        <Image
          style={styles.circuits}
          source={require('@assets/images/machineInfoScreenCircuits_image.png')}
          resizeMode="contain"
        />
        <Text style={styles.minerText}>{translate('miner')}</Text>
        <Text style={styles.nameText}>
          {machine.name} {machine.fraction !== 1 && `- ${f.n}/${f.d}`}
        </Text>
        <Text style={styles.machineTypeText}>
          {machine.machineType} {machine.hashRate}TH
        </Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <View style={{ zIndex: 100 }}>
        <HeaderWithMenuAndProfileButton
          profilePictureUrl={profilePicture}
          openMenu={navigation.openDrawer}
          navigateToProfile={() => navigation.navigate('Profile')}
          alternativeMenu={SMALL_DEVICE ? renderBackButton() : undefined}
        />
      </View>
      {!SMALL_DEVICE && (
        <GoBackField
          style={styles.backContainer}
          zIndex={99}
          goBack={() => navigation.pop()}
        />
      )}
      <View style={styles.content}>
        {renderMachineNameContainer()}
        <View style={styles.infoContainer}>
          <View style={{ paddingLeft: '11.5%' }}>
            <Text style={styles.productionText}>{translate('production')}</Text>
          </View>
          <View style={styles.dataContainer}>
            <View style={styles.graphContainer}>
              <Graph value={remainingDays} max={totalDays} />
            </View>
            <View style={styles.infoFieldsContainer}>
              <InfoField
                title={translate('total')}
                value={Formatter.format(machine.production)}
              />
              <InfoField
                title={translate('daily')}
                value={Formatter.format(machine.dailyProduction)}
              />
              <InfoField
                title={translate('average')}
                value={Formatter.format(machine.averageDailyProduction)}
              />
            </View>
          </View>
        </View>
      </View>
      <Footer startDate={startDate} endDate={endDate} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#27B093',
  },
  content: {
    flex: SMALL_DEVICE ? 0.9 : 1,
    width: '100%',
    alignItems: 'center',
    maxHeight: 400,
  },
  backContainer: {
    alignItems: 'center',
    paddingLeft: '10%',
    flexDirection: 'row',
  },
  nameContainer: {
    width: '100%',
    flex: 1,
    paddingLeft: '10%',
    paddingTop: SMALL_DEVICE ? '5%' : '10%',
    paddingBottom: SMALL_DEVICE ? '8%' : '0%',
  },
  circuits: {
    position: 'absolute',
    width: '100%',
    height: 300,
    right: -30,
    top: '-250%',
  },
  minerText: {
    ...Typography.subtitle,
    textTransform: 'uppercase',
    letterSpacing: 2,
    fontSize: 18,
  },
  infoContainer: {
    width: '100%',
    flex: 4,
    paddingRight: '10%',
    justifyContent: 'center',
  },
  dataContainer: {
    ...(Views.center as object),
    flexDirection: 'row',
    width: '100%',
  },
  nameText: {
    color: '#3DFF39',
    textTransform: 'uppercase',
    fontSize: normalizeText(24),
    fontFamily: 'Archivo-Regular',
  },
  machineTypeText: {
    color: '#3DFF39',
    textTransform: 'uppercase',
    fontSize: normalizeText(15),
    fontFamily: 'Archivo-Regular',
  },
  graphContainer: {
    flex: 7,
    ...(Views.center as object),
    paddingHorizontal: '7%',
  },
  infoFieldsContainer: {
    flex: 5,
    height: '100%',
    justifyContent: 'center',
  },
  profilePictureContainer: {
    height: '55%',
    aspectRatio: 1,
    ...(Views.center as object),
  },
  profilePicture: {
    height: '100%',
    width: '100%',
    borderRadius: 1000,
  },
  productionText: {
    ...Typography.subtitle,
    textTransform: 'uppercase',
    letterSpacing: 2,
    fontSize: 18,
  },
});

const getDaysBetween = (first: Date, second: Date): number => {
  return (second.getTime() - first.getTime()) / (1000 * 3600 * 24);
};

export default connector(MachinesInfoScreen);
