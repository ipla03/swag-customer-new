import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import Header from '@components/Header';
import Icon from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { Typography, Views } from '@styles';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootState } from '@redux/store';
import { connect, ConnectedProps } from 'react-redux';
import { actions as UserActions } from '@redux/slices/UserSlice';
import { actions as LoginActions } from '@redux/slices/LoginScreenSlice';
import Config from 'react-native-config';
import * as SecureStore from 'expo-secure-store';

//#region Redux
interface OwnProps {
  navigation: StackNavigationProp<RootStackNavigatorParamList, 'Profile'>;
}

const mapStateToProps = (state: RootState, ownProps: OwnProps) => ({
  userInfo: state.user.userInfo,
  ...ownProps,
});

const mapDispatchToProps = {
  resetLoginScreen: LoginActions.reset,
  resetUserInfo: UserActions.reset,
};

const connector = connect(mapStateToProps, mapDispatchToProps);

type reduxProps = ConnectedProps<typeof connector>;

type Props = reduxProps;
//#endregion

function ProfileScreen({
  navigation,
  userInfo,
  resetLoginScreen,
  resetUserInfo,
}: Props) {
  const logOut = async () => {
    await Promise.all([
      SecureStore.deleteItemAsync(Config.EMAIL_SECURE_STORE_KEY),
      SecureStore.deleteItemAsync(Config.PASSWORD_SECURE_STORE_KEY),
    ]);

    resetLoginScreen();
    resetUserInfo();
  };

  const renderBackButton = () => {
    return (
      <View style={Views.fillAndCenter}>
        <TouchableOpacity onPress={() => navigation.pop()}>
          <Icon name="ios-arrow-back" color="#ffffff" size={30} />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Header leftButton={renderBackButton()} />
      <View style={styles.content}>
        <View style={styles.profilePictureContainer}>
          <View style={[Views.center, { width: '100%', aspectRatio: 1 }]}>
            <Image
              style={styles.profilePicture}
              source={{ uri: userInfo?.profilePicture }}
              resizeMode="contain"
            />
          </View>
        </View>
        <View style={styles.userInfoContainer}>
          <Text style={styles.nameText}>
            {userInfo?.firstName} {userInfo?.lastName}
          </Text>
          <Text style={styles.usernameText}>{userInfo?.username}</Text>
          <TouchableOpacity onPress={logOut}>
            <View style={styles.logOutContainer}>
              <AntDesign name="logout" color="#999" size={24} />
              <Text style={styles.logoutText}>logout</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  content: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  profilePictureContainer: {
    width: '100%',
    flex: 1,
    paddingHorizontal: 100,
    paddingTop: 50,
    paddingBottom: 30,
  },
  profilePicture: {
    height: '100%',
    width: '100%',
    borderRadius: 1000,
  },
  userInfoContainer: {
    flex: 2,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  nameText: {
    ...Typography.title,
    marginBottom: 10,
  },
  usernameText: {
    ...Typography.secondary,
    fontSize: 17,
  },
  logOutContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20,
  },
  logoutText: {
    fontFamily: 'Archivo-Medium',
    fontSize: 13,
    color: '#999',
    marginLeft: 5,
    textTransform: 'uppercase',
  },
});

export default connector(ProfileScreen);
