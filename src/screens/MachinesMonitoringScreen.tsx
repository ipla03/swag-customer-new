import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import ScreenFooter from '@components/Footer';
import Header from '@components/Header';
import { Colors, Typography, Views } from '@styles';
import { DrawerNavigationProp } from '@react-navigation/drawer';
import { RootState } from '@redux/store';
import { connect, ConnectedProps } from 'react-redux';
import ProfileButton from '@assets/images/profile_button.svg';
import MenuButton from '@assets/images/menu_button.svg';
import AddMachineCard from '@components/machineCards/AddMachineCard';
import Footer from '@components/machinesMonitoringScreen/Footer';
import MachineCard from '@components/machineCards/MachineCard';
import Search from '@components/machinesMonitoringScreen/Search';
import { StackNavigationProp } from '@react-navigation/stack';
import { getUserMachines } from '@apis/getUserMachines';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { translate } from '@utils/translation/translations';

//#region Redux
interface OwnProps {
  navigation: DrawerNavigationProp<RootDrawerNavigatorParamList, 'Home'> &
    StackNavigationProp<MachinesMonitoringScreenStack, 'Monitoring'> &
    StackNavigationProp<RootStackNavigatorParamList, 'RootDrawer'>;
}

const mapStateToProps = (state: RootState, ownProps: OwnProps) => ({
  userInfo: state.user.userInfo,
  userMachines: state.user.userMachines.list,
  userMachinesStatus: state.user.userMachines.status,
  userMachinesError: state.user.userMachines.error,
  ...ownProps,
});

const dispatchProps = {
  getUserMachines: getUserMachines,
};

const connector = connect(mapStateToProps, dispatchProps);

type reduxProps = ConnectedProps<typeof connector>;

type Props = reduxProps;
//#endregion

function MachinesMonitoringScreen({
  navigation,
  userInfo,
  userMachines,
  getUserMachines,
  userMachinesStatus,
  userMachinesError,
}: Props) {
  const [searchFilter, setSearchFilter] = useState('');
  const searchedMachines =
    userMachines?.filter(
      m =>
        searchFilter === '' ||
        m.name.toUpperCase().includes(searchFilter.toUpperCase()),
    ) || [];

  useEffect(() => {
    getUserMachines();
  }, []);

  const renderProfileButton = () => {
    return (
      <View style={Views.fillAndCenter}>
        <TouchableOpacity
          style={Views.fillAndCenter}
          onPress={() => navigation.navigate('Profile')}>
          <View style={Views.fillAndCenter}>
            <ProfileButton
              style={{ position: 'absolute' }}
              width="60%"
              height="60%"
            />
            <View style={styles.profilePictureContainer}>
              <Image
                style={styles.profilePicture}
                source={{ uri: userInfo?.profilePicture }}
                resizeMode="contain"
              />
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  const renderDrawerButton = () => {
    return (
      <View style={Views.fillAndCenter}>
        <TouchableOpacity onPress={() => navigation.openDrawer()}>
          <MenuButton width={24} height={24} />
        </TouchableOpacity>
      </View>
    );
  };

  function openInfoScreenFromCard(m: Machine) {
    const targetScreen = m.status === "PRODUCING" ? 'Info' : 'MachineNotStarted';
    navigation.navigate(targetScreen, { machine: m });
  }

  const renderMachinesList = () => {
    if (searchedMachines.length === 0) {
      return;
    }

    return searchedMachines.length > 0 ? (
      searchedMachines.map(m => (
        <MachineCard
          machine={m}
          key={m.id}
          openMachineInfoScreen={openInfoScreenFromCard}
        />
      ))
    ) : (
      <View style={[Views.center, { width: '100%', height: 100 }]}>
        <Text style={styles.noProductsText}>
          {userMachines && userMachines?.length > 0
            ? translate('monitoringScreenNoMachinesForSearch')
            : translate('monitoringScreenNoMachinesAvailable')}
        </Text>
      </View>
    );
  };

  const renderLoading = () => {
    return (
      <View style={{ marginVertical: 50 }}>
        <ActivityIndicator color="#27B093" />
      </View>
    );
  };

  const renderError = () => {
    return (
      <View style={{ marginVertical: 30, alignItems: 'center' }}>
        <Text
          style={[Typography.subtitle, { color: '#27B093', marginBottom: 20 }]}>
          {userMachinesError}
        </Text>
        <TouchableOpacity onPress={getUserMachines}>
          <AntDesign name="reload1" size={25} color="#27B093" />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <ScrollView style={Views.fill}>
      <View style={styles.container}>
        <Header
          leftButton={renderDrawerButton()}
          rightButton={renderProfileButton()}
        />
        <View style={styles.content}>
          <View
            style={{ width: '100%', alignItems: 'center', marginVertical: 20 }}>
            {searchedMachines.length > 0 && (
              <>
                <Text style={styles.titleText}>
                  {translate('monitoringScreenTitle')}
                </Text>
                <Text style={styles.subTitleText}>
                  {translate('selectMachine')}
                </Text>
                <Search value={searchFilter} setSearch={setSearchFilter} />
              </>
            )}
            {(() => {
              switch (userMachinesStatus) {
                case 'idle':
                case 'succeed':
                  return renderMachinesList();
                case 'loading':
                  return renderLoading();
                case 'failed':
                  return renderError();
              }
            })()}
            <AddMachineCard
              style={{ marginBottom: searchedMachines.length > 0 ? 20 : 0 }}
            />
          </View>
          {(userMachinesStatus === 'loading' ||
            searchedMachines.length === 0) && <Footer />}
        </View>
        <ScreenFooter />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
  },
  profilePictureContainer: {
    height: '55%',
    aspectRatio: 1,
    ...(Views.center as object),
  },
  profilePicture: {
    height: '100%',
    width: '100%',
    borderRadius: 1000,
  },
  titleText: {
    ...Typography.title,
    color: '#27B093',
    alignSelf: 'flex-start',
    marginLeft: '10%',
    marginTop: 10,
  },
  subTitleText: {
    ...Typography.subtitle,
    color: Colors.primary,
    alignSelf: 'flex-start',
    marginLeft: '10%',
    marginBottom: 10,
  },
  noProductsText: {
    ...Typography.subtitle,
    color: '#8395a7',
  },
});

export default connector(MachinesMonitoringScreen);
