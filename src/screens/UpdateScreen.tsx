import { StackNavigationProp } from '@react-navigation/stack';
import Constants from 'expo-constants';
import React, { useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  Linking,
  Platform,
} from 'react-native';
import Button from '@components/Button';
import Header from '@components/Header';
import { Colors } from '@styles';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;
const ASPECT_RATIO = HEIGHT / WIDTH;

interface OwnProps {
  navigation: StackNavigationProp<LoginStackNavigatorParamList, 'Login'>;
}

type Props = OwnProps;

function UpdateScreen({ navigation }: Props) {
  useEffect(() => {
    navigation.addListener('beforeRemove', e => {
      e.preventDefault();
    });
  }, []);

  const goToStore = () => {
    const url =
      Platform.OS === 'android'
        ? 'https://play.google.com/store/apps/details?id=com.swaggy.clientapp'
        : 'itms-apps://itunes.apple.com/us/app/1558335117?mt=8';
    Linking.openURL(url);
  };

  return (
    <View style={styles.container}>
      <Header />
      <View style={styles.contentContainer}>
        <Image
          style={styles.updateImage}
          source={require('@assets/images/update_image.png')}
          resizeMode="contain"
        />
        <View style={styles.textContainer}>
          <Text style={styles.updateText}>Update your app</Text>
          <Text style={styles.text}>
            To enjoy our newest features tap the button below
          </Text>
        </View>
        <View style={styles.buttonContainer}>
          <Button
            onPress={goToStore}
            height={53}
            width="100%"
            label="Update"
            color="#009FE3"
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  contentContainer: {
    width: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  updateText: {
    color: Colors.primary,
    fontFamily: 'Archivo-Medium',
    fontSize: 28,
    marginBottom: 10,
  },
  text: {
    color: '#608194',
    fontFamily: 'Archivo-Medium',
    fontSize: 18,
    textAlign: 'center',
    marginHorizontal: '10%',
  },
  updateImage: {
    height: HEIGHT * 0.3,
    marginTop: ASPECT_RATIO > 1.9 ? 85 : 45,
  },
  textContainer: {
    flex: 3,
    paddingTop: 30,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  buttonContainer: {
    flex: 3,
    width: '80%',
  },
});

export default UpdateScreen;
