import { StackNavigationProp } from "@react-navigation/stack";
import React, { useEffect } from "react";
import { View, Text, StyleSheet, Image, Dimensions } from "react-native";
import { connect, ConnectedProps } from "react-redux";
import Header from "@components/Header";
import { Colors } from "@styles";
import { RootState } from "@redux/store";

const HEIGHT = Dimensions.get("window").height;

//#region Redux
interface OwnProps {
  navigation: StackNavigationProp<LoginStackNavigatorParamList, "Maintenance">;
}

const mapStateToProps = (state: RootState, ownProps: OwnProps) => ({
  infoText: state.appInfo.configuration.data?.maintenance?.info,
  ...ownProps,
});

const connector = connect(mapStateToProps);

type reduxProps = ConnectedProps<typeof connector>;

type Props = reduxProps;
//#endregion

function MaintenanceScree({ navigation, infoText }: Props) {
  useEffect(() => {
    navigation.addListener("beforeRemove", (e) => {
      e.preventDefault();
    });
  }, []);

  return (
    <View style={styles.container}>
      <Header />
      <View style={styles.contentContainer}>
        <Image
          style={styles.maintenanceImage}
          source={require("@assets/images/maintenanceImage.png")}
          resizeMode="contain"
        />
        <View style={styles.textContainer}>
          <Text style={styles.maintenanceText}>Under Maintenance</Text>
          <Text style={styles.text}>{infoText}</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-start",
  },
  contentContainer: {
    width: "100%",
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  maintenanceText: {
    color: Colors.primary,
    fontFamily: "Archivo-Medium",
    fontSize: 28,
    marginBottom: 10,
  },
  text: {
    color: "#608194",
    fontFamily: "Archivo-Medium",
    fontSize: 18,
    textAlign: "center",
    marginHorizontal: "10%",
  },
  maintenanceImage: {
    height: HEIGHT * 0.3,
    marginTop: 85,
  },
  textContainer: {
    flex: 1,
    paddingTop: 30,
    alignItems: "center",
    justifyContent: "flex-start",
  },
});

export default connector(MaintenanceScree);
