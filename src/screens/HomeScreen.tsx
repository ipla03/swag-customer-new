import { RootState } from '@redux/store';
import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { connect, ConnectedProps } from 'react-redux';
import Header from '@components/Header';
import Footer from '@components/Footer';
import { Typography, Views } from '@styles';
import ProfileButton from '@assets/images/profile_button.svg';
import MenuButton from '@assets/images/menu_button.svg';
import { DrawerNavigationProp } from '@react-navigation/drawer';
import { translate } from '@utils/translation/translations';

//#region Redux
interface OwnProps {
  navigation: DrawerNavigationProp<RootDrawerNavigatorParamList, 'Home'>;
}

const mapStateToProps = (state: RootState, ownProps: OwnProps) => ({
  userInfo: state.user.userInfo,
  ...ownProps,
});

const connector = connect(mapStateToProps);

type reduxProps = ConnectedProps<typeof connector>;

type Props = reduxProps;
//#endregion

function HomeScreen({ userInfo, navigation }: Props) {
  const renderProfileButton = () => {
    return (
      <View style={Views.fillAndCenter}>
        <TouchableOpacity
          style={Views.fillAndCenter}
          onPress={() => navigation.navigate('Profile')}>
          <View style={Views.fillAndCenter}>
            <ProfileButton
              style={{ position: 'absolute' }}
              width="60%"
              height="60%"
            />
            <View style={styles.profilePictureContainer}>
              <Image
                style={styles.profilePicture}
                source={{ uri: userInfo?.profilePicture }}
                resizeMode="contain"
              />
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  const renderDrawerButton = () => {
    return (
      <View style={Views.fillAndCenter}>
        <TouchableOpacity onPress={() => navigation.openDrawer()}>
          <MenuButton width={24} height={24} />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Header
        leftButton={renderDrawerButton()}
        rightButton={renderProfileButton()}
      />
      <View style={styles.content}>
        <View style={[Views.center, { flex: 1 }]}>
          <Text style={Typography.title}>
            <Text style={styles.greetingsText}>
              {translate('homeGreetings')}
            </Text>{' '}
            {userInfo?.firstName}
          </Text>
        </View>
        <View style={[Views.center, { flex: 3 }]}>
          <Image
            style={styles.image}
            source={require('@assets/images/home_image.png')}
            resizeMode="contain"
          />
        </View>
        <View style={[Views.center, { flex: 1 }]}>
          <Text style={styles.descriptionText}>
            {translate('homeDescription')}
          </Text>
        </View>
      </View>
      <Footer />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    overflow: 'hidden',
  },
  content: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  greetingsText: {
    ...Typography.subtitle,
    fontSize: Typography.title.fontSize,
  },
  descriptionText: {
    ...Typography.subtitle,
    textAlign: 'center',
    marginHorizontal: 50,
  },
  image: {
    height: '85%',
  },
  profilePictureContainer: {
    height: '55%',
    aspectRatio: 1,
    ...(Views.center as object),
  },
  profilePicture: {
    height: '100%',
    width: '100%',
    borderRadius: 1000,
  },
});

export default connector(HomeScreen);
