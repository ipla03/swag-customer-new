export class AppInfoException extends Error {
  constructor(message: string) {
    super(message);
    this.name = "AppInfoException";
  }
}
