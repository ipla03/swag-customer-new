import { LoginException } from "./loginException";

export class EmailNotValidException extends LoginException {
  constructor(message: string) {
    super(message);
    this.name = "EmailNotValidException";
  }
}
