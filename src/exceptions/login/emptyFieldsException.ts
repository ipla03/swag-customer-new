import { LoginException } from "./loginException";

export class EmptyFieldException extends LoginException {
  constructor(message: string) {
    super(message);
    this.name = "EmptyFieldException";
  }
}
