import * as Colors from "./colors";

export const title = {
  fontFamily: "Archivo-Medium",
  fontSize: 24,
  color: Colors.primary,
};

export const subtitle = {
  fontFamily: "Archivo-Regular",
  fontSize: 14,
  color: Colors.primary,
};

export const secondary = {
  fontFamily: "Archivo-SemiBold",
  fontSize: 11,
  color: Colors.gray,
};

export const error = {
  fontFamily: "Archivo-Regular",
  fontSize: 14,
  color: "#ff6b6b",
};
