import { StyleProp, ViewStyle } from "react-native";

export const center: StyleProp<ViewStyle> = {
  alignItems: "center",
  justifyContent: "center",
};

export const fill: StyleProp<ViewStyle> = {
  height: "100%",
  width: "100%",
};

export const fillAndCenter: StyleProp<ViewStyle> = {
  ...center,
  ...fill,
};
