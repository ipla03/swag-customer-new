import { RootState } from '@redux/store';
import { AppInfoException } from '../exceptions/appInfoException';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { ThunkApiConfiguration } from 'src/types/thunkApiConfiguration';
import axios from './axiosClientWithInterceptor';
import { AxiosResponse } from 'axios';
import Config from 'react-native-config';

export const getUserMachines = createAsyncThunk<
  Machine[],
  undefined,
  ThunkApiConfiguration
>('user/machines', async (_, thunkApi) => {
  const { getState, rejectWithValue } = thunkApi;
  const state: RootState = getState();

  try {
    const machines = await getMachines(
      state.user.userInfo!.id,
      state.user.jwt!,
    );

    machines.sort((firstDate, secondDate) => {
      return (
        Date.parse(firstDate.miningStartDate) -
        Date.parse(secondDate.miningStartDate)
      );
    });

    return machines;
  } catch (error) {
    if (error instanceof AppInfoException) {
      return rejectWithValue(error.message);
    }

    console.log(error);
    return rejectWithValue('Unable to get user machines');
  }
});

const getMachines = async (userId: string, jwt: string): Promise<Machine[]> => {
  console.log(Config.CLIENT_MODULE_ENDPOINT);
  const response: AxiosResponse<Machine[]> = await axios.get(
    Config.CLIENT_MODULE_ENDPOINT + '/v1/machine/byUser?userId=' + userId,
    {
      headers: {
        Authorization: 'Bearer ' + jwt,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    },
  );

  console.log(response.data);

  return response.data;
};
