import { Config } from 'react-native-config';
import { EmptyFieldException } from './../exceptions/login/emptyFieldsException';
import { LoginException } from './../exceptions/login/loginException';
import { EmailNotValidException } from '../exceptions/login/emailNotValidException';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { ThunkApiConfiguration } from 'src/types/thunkApiConfiguration';
import { AxiosError, AxiosResponse } from 'axios';
import axios from './axiosClientWithInterceptor';
import { actions as UserActions } from '@redux/slices/UserSlice';
import * as SecureStore from 'expo-secure-store';
import { registerUserOnNotificationModule } from '@services/notificationService';

interface UserLoginFields {
  email: string;
  password: string;
}

export const loginUser = createAsyncThunk<
  void,
  UserLoginFields,
  ThunkApiConfiguration
>('loginScreen/login', async (userFields: UserLoginFields, thunkApi) => {
  const { dispatch, rejectWithValue } = thunkApi;

  try {
    if (!isValidEmail(userFields.email)) {
      throw new EmailNotValidException('Email not valid');
    }

    if (!areFieldsFilled(userFields)) {
      throw new EmptyFieldException('All fields must be filled');
    }

    const jwt = await getJwt(userFields.email, userFields.password);

    dispatch(UserActions.setJwt(jwt));

    const userInfo = await getUserInfo(userFields.email, jwt);

    registerUserOnNotificationModule(userInfo.id, jwt);

    saveUserCredentials(userFields.email, userFields.password);

    dispatch(UserActions.setUserInfo(userInfo));
    dispatch(UserActions.setIsLogged(true));
  } catch (error) {
    if (error instanceof LoginException) {
      return rejectWithValue(error.message);
    }

    console.log(error);
    return rejectWithValue('Unknown error');
  }
});

const getJwt = async (email: string, password: string): Promise<string> => {
  try {
    const response: AxiosResponse<GetJwtResponse> = await axios.post(
      Config.USER_MODULE_ENDPOINT + '/public/login',
      {
        username: email,
        password: password,
      },
      {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      },
    );

    return response.data.jwt;
  } catch (err) {
    if (err.response) {
      if ((err as AxiosError).response?.status === 401) {
        throw new LoginException('Wrong email or password');
      }
    }
    throw err;
  }
};

const getUserInfo = async (
  username: string,
  token: string,
): Promise<UserInfo> => {
  try {
    const response: AxiosResponse<UserInfo> = await axios.get(
      Config.CLIENT_MODULE_ENDPOINT + '/v1/user/' + username,
      {
        headers: {
          Authorization: 'Bearer ' + token,
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      },
    );

    return response.data;
  } catch (err) {
    if (err.response) {
      if ((err as AxiosError).response?.status === 404) {
        throw new LoginException('Unable to find the user');
      }
    }

    throw err;
  }
};

const saveUserCredentials = (email: string, password: string) => {
  SecureStore.setItemAsync(Config.EMAIL_SECURE_STORE_KEY, email);
  SecureStore.setItemAsync(Config.PASSWORD_SECURE_STORE_KEY, password);
};

const isValidEmail = (email: string): boolean => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

const areFieldsFilled = (field: UserLoginFields): boolean => {
  const values: string[] = Object.values(field);
  for (let i = 0; i < values.length; i++) {
    if (values[i].trim() === '') {
      return false;
    }
  }
  return true;
};
