import { AppInfoException } from '../exceptions/appInfoException';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { ThunkApiConfiguration } from 'src/types/thunkApiConfiguration';
import axios from './axiosClientWithInterceptor';
import { AxiosError, AxiosResponse } from 'axios';
import Config from 'react-native-config';
import store from '@redux/store';
import { actions as AppInfoActions } from '@redux/slices/AppInfoSlice';
import DeviceInfo from 'react-native-device-info';
import Configuration from 'react-native-config';
import { Platform } from 'react-native';

export const getAppConfiguration = createAsyncThunk<
  AppConfiguration,
  undefined,
  ThunkApiConfiguration
>('appInfo/configuration', async (_, thunkApi) => {
  const { rejectWithValue } = thunkApi;

  try {
    const appConfig = await _getAppInfo();
    return appConfig;
  } catch (error) {
    if (error instanceof AppInfoException) {
      return rejectWithValue(error.message);
    }

    console.log(error);
    return rejectWithValue('Unable to establish a connection with the server');
  }
});

const _getAppInfo = async (): Promise<AppConfiguration> => {
  try {
    const response: AxiosResponse<AppConfiguration> = await axios.get(
      Config.CLIENT_MODULE_ENDPOINT + '/public/v1/info?platform=' + Platform.OS,
      {
        headers: {
          apikey: Configuration.SERVER_API_KEY,
        },
      },
    );

    validateCurrentVersion(DeviceInfo.getVersion(), response.data.minVersion!);

    return response.data;
  } catch (error) {
    if (error.response) {
      if ((error as AxiosError).response?.status === 503) {
        return {
          maintenance: {
            enabled: true,
            info:
              'The application is currently under maintenance and will be back soon',
          },
        };
      }
    }

    throw error;
  }
};

const validateCurrentVersion = (currentVersion: string, minVersion: string) => {
  const currentVersionValues = currentVersion.split('.');
  const minVersionsValues = minVersion.split('.');

  for (let i = 0; i < minVersion.length; i++) {
    const min = parseInt(minVersionsValues[i]);
    const current = parseInt(currentVersionValues[i]);

    if (min > current) {
      store.dispatch(AppInfoActions.setNeedUpdate(true));
      return;
    }
  }
};
