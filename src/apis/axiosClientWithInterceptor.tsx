import axios, { AxiosInstance } from 'axios';
import store from '@redux/store';
import { actions as UserActions } from '@redux/slices/UserSlice';
import { actions as LoginActions } from '@redux/slices/LoginScreenSlice';
import * as SecureStore from 'expo-secure-store';
import Config from 'react-native-config';

const axiosWithInterceptor: AxiosInstance = axios.create({
  timeout: 10000,
});

axiosWithInterceptor.interceptors.response.use(async response => {
  if (response.status === 403) {
    console.log('Some api call returned 403, logging out the user');

    await Promise.all([
      SecureStore.deleteItemAsync(Config.EMAIL_SECURE_STORE_KEY),
      SecureStore.deleteItemAsync(Config.PASSWORD_SECURE_STORE_KEY),
    ]);

    store.dispatch(UserActions.reset());
    store.dispatch(LoginActions.reset());
  }

  return response;
});

export default axiosWithInterceptor;
