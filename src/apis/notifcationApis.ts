import { AxiosError } from 'axios';
import Config from 'react-native-config';
import axios from './axiosClientWithInterceptor';
import * as SecureStore from 'expo-secure-store';
import AsyncStorage from '@react-native-async-storage/async-storage';

const secureStoreNotificationTokenKey = 'REGISTERED_NOTIFICATION_TOKEN';
const registeredUserStorageKey = 'HAS_REGISTRED_USER_FOR_NOTIFICATIONS';

export async function registerUser(
  userId: string,
  firebaseToken: string,
  jwt: string,
) {
  const hasRegisteredUser = await AsyncStorage.getItem(
    registeredUserStorageKey,
  );
  if (hasRegisteredUser) {
    return;
  }

  try {
    await axios.post(
      Config.NOTIFICATION_MODULE_ENDPOINT + '/v1/user',
      {
        id: userId,
        tags: [],
        devices: [
          {
            userId: userId,
            firebaseToken: firebaseToken,
            project: Config.PROJECT_NAME,
          },
        ],
      },
      {
        headers: {
          Authorization: 'Bearer ' + jwt,
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      },
    );

    AsyncStorage.setItem(registeredUserStorageKey, 'true');
  } catch (ex) {
    if (ex.response) {
      if ((ex as AxiosError).response?.status === 409) {
        registerDeviceToken(userId, firebaseToken, jwt);
      }
    }
    return;
  }
}

export async function registerDeviceToken(
  userId: string,
  firebaseToken: string,
  jwt: string,
) {
  const registeredToken = await SecureStore.getItemAsync(
    secureStoreNotificationTokenKey,
  );

  if (registeredToken || registeredToken === firebaseToken) {
    return;
  }

  try {
    await axios.post(
      Config.NOTIFICATION_MODULE_ENDPOINT + '/v1/userDevice/register',
      {
        userId,
        firebaseToken,
        project: Config.PROJECT_NAME,
      },
      {
        headers: {
          Authorization: 'Bearer ' + jwt,
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      },
    );

    SecureStore.setItemAsync(secureStoreNotificationTokenKey, firebaseToken);
  } catch (ex) {
    if (ex.response) {
      if ((ex as AxiosError).response?.status === 409) {
        SecureStore.setItemAsync(
          secureStoreNotificationTokenKey,
          firebaseToken,
        );
        AsyncStorage.setItem(registeredUserStorageKey, 'true');
      }
    }
    return;
  }
}
