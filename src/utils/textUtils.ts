import { Dimensions, PixelRatio, Platform } from "react-native";

const WIDTH = Dimensions.get("screen").width;
const scale = WIDTH / 320;

export function normalizeText(size: number) {
  const newSize = size * scale;
  if (Platform.OS === "ios") {
    return Math.round(PixelRatio.roundToNearestPixel(newSize));
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
  }
}
