import 'intl';
import 'intl/locale-data/jsonp/it';

export default new Intl.NumberFormat('it-IT', {
  style: 'decimal',
  maximumFractionDigits: 8,
  minimumFractionDigits: 8,
});
