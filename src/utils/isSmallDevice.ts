import { Dimensions } from 'react-native';

let _isSmallDevice: boolean;

export function isSmallDevice() {
  if (!_isSmallDevice) {
    const { height, width } = Dimensions.get('screen');
    const ratio = height / width;
    _isSmallDevice = ratio < 1.85;
  }
  return _isSmallDevice;
}
