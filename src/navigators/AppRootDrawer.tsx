import { createDrawerNavigator } from "@react-navigation/drawer";
import React from "react";
import CustomDrawer from "@components/customDrawer/CustomDrawer";

import HomeScreen from "@screens/HomeScreen";
import MachinesMonitoringStack from "@navigators/MachinesMonitoringStack";

const Drawer = createDrawerNavigator<RootDrawerNavigatorParamList>();

function AppRootDrawer() {
  return (
    <Drawer.Navigator
      drawerContent={(props) => <CustomDrawer navigation={props.navigation} />}
      drawerStyle={{ backgroundColor: "transparent", width: "70%" }}
    >
      <Drawer.Screen name="Home" component={HomeScreen} />
      <Drawer.Screen
        name="MachinesMonitoring"
        component={MachinesMonitoringStack}
      />
    </Drawer.Navigator>
  );
}

export default AppRootDrawer;
