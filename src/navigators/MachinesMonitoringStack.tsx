import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import MachinesMonitoringScreen from '@screens/MachinesMonitoringScreen';
import MachinesInfoScreen from '@screens/MachinesInfoScreen';
import MachineNotStartedScreen from '@screens/MachineNotStartedScreen';

const Stack = createStackNavigator<MachinesMonitoringScreenStack>();

function MachineMonitoringStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Monitoring"
        component={MachinesMonitoringScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Info"
        component={MachinesInfoScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="MachineNotStarted"
        component={MachineNotStartedScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}

export default MachineMonitoringStack;
