import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import AppRootDrawer from "@navigators/AppRootDrawer";
import ProfileScreen from "@screens/ProfileScreen";

const Stack = createStackNavigator<RootStackNavigatorParamList>();

function AppRootStack() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="RootDrawer"
          component={AppRootDrawer}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Profile"
          component={ProfileScreen}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default AppRootStack;
