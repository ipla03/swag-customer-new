import React, { useEffect, useState } from 'react';
import { Provider } from 'react-redux';
import Store from '@redux/store';
import AuthSwitcher from '@components/AuthSwitcher';
import { Alert, StatusBar, Text } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import * as RNLocalize from 'react-native-localize';
import { setI18nConfig, translate } from '@utils/translation/translations';
import useForceUpdate from '@utils/useForceUpdate';

import '@services/notificationService';

/**
 * Disable Text scaling in order to prevent size problems
 */
//@ts-ignore
Text.defaultProps = Text.defaultProps || {};
//@ts-ignore
Text.defaultProps.allowFontScaling = false;

function App() {
  const forceUpdate = useForceUpdate();
  setI18nConfig();

  useEffect(() => {
    RNLocalize.addEventListener('change', handleLocalizationChange);

    return () => {
      RNLocalize.removeEventListener('change', handleLocalizationChange);
    };
  }, []);

  const handleLocalizationChange = () => {
    setI18nConfig();
    forceUpdate();
  };

  return (
    <SafeAreaProvider>
      <Provider store={Store}>
        <StatusBar
          translucent={true}
          backgroundColor="transparent"
          barStyle="light-content"
        />
        <AuthSwitcher />
      </Provider>
    </SafeAreaProvider>
  );
}

export default App;
