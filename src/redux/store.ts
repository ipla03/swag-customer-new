import { configureStore } from "@reduxjs/toolkit";

//Import all slice
import { loginScreenSlice } from "./slices/LoginScreenSlice";
import { userSlice } from "./slices/UserSlice";
import { appInfoSlice } from "./slices/AppInfoSlice";
//--------------------------------

const store = configureStore({
  reducer: {
    loginScreen: loginScreenSlice.reducer,
    user: userSlice.reducer,
    appInfo: appInfoSlice.reducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;

export default store;
