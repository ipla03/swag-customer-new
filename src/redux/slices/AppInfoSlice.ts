import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { getAppConfiguration } from "@apis/getAppConfiguration";

interface Configuration {
  status: ApiResponseStatus;
  error?: string;
  data?: AppConfiguration;
}

interface State {
  configuration: Configuration;
  needUpdate: boolean;
}

const initialState: State = {
  configuration: {
    status: "idle",
  },
  needUpdate: false,
};

export const appInfoSlice = createSlice({
  name: "appInfo",
  initialState: initialState,
  reducers: {
    setNeedUpdate: (state, action: PayloadAction<boolean>) => {
      state.needUpdate = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getAppConfiguration.fulfilled, (state, action) => {
      state.configuration.data = action.payload;
      state.configuration.status = "succeed";
    });
    builder.addCase(getAppConfiguration.rejected, (state, action) => {
      state.configuration.status = "failed";
      state.configuration.error = action.payload || "Unknown error";
    });
    builder.addCase(getAppConfiguration.pending, (state, _) => {
      state.configuration.status = "loading";
    });
  },
});

export const actions = appInfoSlice.actions;
