import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { loginUser } from "@apis/userLogin";

interface State {
  status: ApiResponseStatus;
  error: string;
}

const initialState: State = {
  status: "idle",
  error: "",
};

export const loginScreenSlice = createSlice({
  name: "loginScreen",
  initialState: initialState,
  reducers: {
    reset: (state, _: PayloadAction<undefined>) => {
      state.status = initialState.status;
      state.error = initialState.error;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(loginUser.fulfilled, (state, _) => {
      state.status = "succeed";
    });
    builder.addCase(loginUser.rejected, (state, action) => {
      state.status = "failed";
      state.error = action.payload || "Unknown error";
    });
    builder.addCase(loginUser.pending, (state, _) => {
      state.status = "loading";
    });
  },
});

export const actions = loginScreenSlice.actions;
