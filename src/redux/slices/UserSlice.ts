import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { getUserMachines } from "@apis/getUserMachines";

interface Machines {
  status: ApiResponseStatus;
  error?: string;
  list?: Machine[];
}

interface State {
  jwt?: string;
  userInfo?: UserInfo;
  userMachines: Machines;
  isLogged: boolean;
}

const initialState: State = {
  jwt: undefined,
  userInfo: undefined,
  isLogged: false,
  userMachines: { status: "idle" },
};

export const userSlice = createSlice({
  name: "user",
  initialState: initialState,
  reducers: {
    setJwt: (state, action: PayloadAction<string>) => {
      state.jwt = action.payload;
    },
    setUserInfo: (state, action: PayloadAction<UserInfo>) => {
      state.userInfo = action.payload;
    },
    setIsLogged: (state, action: PayloadAction<boolean>) => {
      state.isLogged = action.payload;
    },
    reset: (state, _: PayloadAction<undefined>) => {
      state.jwt = initialState.jwt;
      state.userInfo = initialState.userInfo;
      state.isLogged = false;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getUserMachines.fulfilled, (state, action) => {
      state.userMachines.status = "succeed";
      state.userMachines.list = action.payload;
    });
    builder.addCase(getUserMachines.rejected, (state, action) => {
      state.userMachines.status = "failed";
      state.userMachines.error = action.payload || "Unknown error";
    });
    builder.addCase(getUserMachines.pending, (state, _) => {
      state.userMachines.status = "loading";
    });
  },
});

export const actions = userSlice.actions;
